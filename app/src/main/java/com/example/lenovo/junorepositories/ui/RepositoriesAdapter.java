package com.example.lenovo.junorepositories.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lenovo.junorepositories.R;

import java.util.ArrayList;

/**
 * Created by Lenovo on 05.06.2018.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.RepoViewHolder> {

    private ArrayList<String> repositories;

    public RepositoriesAdapter(ArrayList<String> repositories) {
        this.repositories = repositories;
    }

    @Override
    public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item, parent, false);
        return new RepoViewHolder(listRow);
    }

    @Override
    public void onBindViewHolder(RepoViewHolder holder, int position) {
        holder.num.setText(String.valueOf(position + 1));
        holder.name.setText(repositories.get(position));
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public class RepoViewHolder extends RecyclerView.ViewHolder {
        TextView num, name;

        private RepoViewHolder(View itemView) {
            super(itemView);
            num = itemView.findViewById(R.id.num);
            name = itemView.findViewById(R.id.name);
        }
    }
}
