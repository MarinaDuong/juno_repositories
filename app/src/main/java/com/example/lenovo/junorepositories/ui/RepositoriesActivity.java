package com.example.lenovo.junorepositories.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.junorepositories.R;
import com.example.lenovo.junorepositories.entities.JunoRepositoriesEntity;
import com.example.lenovo.junorepositories.entities.SearchRepos;
import com.example.lenovo.junorepositories.entities.errors.Errors;
import com.example.lenovo.junorepositories.rxrest.RxRetrofitRestService;
import com.example.lenovo.junorepositories.utils.Consts;
import com.example.lenovo.junorepositories.utils.Helpers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class RepositoriesActivity extends AppCompatActivity {

    private String TAG = "!!!!!RepositoriesActivity";

    private RecyclerView recyclerView;
    private TextView tvRepositories;
    private ProgressBar progressBar;
    private ArrayList<String> reposFromServer = new ArrayList<>();
    private ArrayList<String> reposForAdapter = new ArrayList<>();
    private RepositoriesAdapter adapter;
    private LinearLayoutManager layoutManager;
    private boolean isLoading;
    private boolean isLastPage;
    private int PAGE_SIZE = 30;
    private int currentPage = 0;
    private Subscription subscription;
    private int countLoadedRepos;
    private SearchView searchView;
    private String urla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        tvRepositories = findViewById(R.id.tv_info);
        recyclerView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RepositoriesAdapter(reposForAdapter);
        recyclerView.setAdapter(adapter);

        if (Helpers.hasConnection(getApplicationContext())) {
            rxLoadMoreReposNames();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.no_connection), Toast.LENGTH_LONG).show();
        }
        recyclerView.addOnScrollListener(sl);
    }

    private RecyclerView.OnScrollListener sl = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                    if (Helpers.hasConnection(getApplicationContext())) {
                        rxLoadMoreReposNames();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

    Observable<ArrayList<JunoRepositoriesEntity>> reposObservable(int page) {
        return RxRetrofitRestService.getRepositoriesApi().getRepositories(Consts.ORG, Consts.TYPE, page);
    }

    Observable<SearchRepos> searchReposObservable(String name) {
        return RxRetrofitRestService.getRepositoriesApi().getSearchRepos(name);
    }

    private String getUrla() {
        return urla;
    }

    private void setUrla(String searchText) {
        urla = String.format(Consts.SEARCH_REPOS_URL, searchText);
    }

    private Integer getCountLoadedRepos(ArrayList<JunoRepositoriesEntity> repos) {
        countLoadedRepos = repos.size();
        return countLoadedRepos;
    }

    private void rxLoadMoreReposNames() {
        isLoading = true;
        currentPage += 1;
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        Observable<ArrayList<JunoRepositoriesEntity>> reposObservable = reposObservable(currentPage);
        if (reposObservable != null) {
            subscription = reposObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(junoRepositoriesEntities -> getCountLoadedRepos(junoRepositoriesEntities))
                    .flatMap(junoRepositoriesEntities -> Observable.from(junoRepositoriesEntities))
                    .flatMap(junoRepositoriesEntity -> Helpers.getName(junoRepositoriesEntity))
                    .doOnNext(name -> reposFromServer.add(name))
                    .doOnNext(name -> reposForAdapter.add(name))
                    .doOnNext(name -> tvRepositories.setText(String.format(getResources().getString(R.string.load_repos_count), reposForAdapter.size())))
                    .doOnNext(name -> recyclerView.getAdapter().notifyItemInserted(reposForAdapter.size()))
                    .subscribe(new Action1<String>() {
                        @Override
                        public void call(String s) {
                            if (currentPage == 1) {
                                progressBar.setVisibility(View.GONE);
                            }
                            isLoading = false;
                            if (countLoadedRepos < PAGE_SIZE) {
                                isLastPage = true;
                            }
                        }
                    });

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.unknown_error), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_search);
        menuItem.setOnActionExpandListener(menuItemExpandListener);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint(getString(R.string.search_hint));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(searchViewQueryTextListener);
        return super.onPrepareOptionsMenu(menu);
    }

    private SearchView.OnQueryTextListener searchViewQueryTextListener = new SearchView.OnQueryTextListener() {
        ArrayList<String> searchRepos = new ArrayList<>();

        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            searchRepos.clear();
            if (newText.trim().length() > 0) {
                setUrla(newText);
                loadSearchRepos(getUrla(), searchRepos);
            } else {
                notifyListForAdapter(reposFromServer);
            }
            return false;
        }


    };

    private void notifyListForAdapter(ArrayList<String> arrayList) {
        reposForAdapter.clear();
        reposForAdapter.addAll(arrayList);
        recyclerView.getAdapter().notifyDataSetChanged();
        tvRepositories.setText(String.format(getResources().getString(R.string.search_repos_count), arrayList.size()));
    }

    private MenuItem.OnActionExpandListener menuItemExpandListener = new MenuItem.OnActionExpandListener() {
        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {
            return true;
        }

        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            searchView.setQuery("", false);
            searchView.clearFocus();
            return true;
        }
    };

    private void loadSearchRepos(String text, ArrayList<String> searchRepos) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        Observable<SearchRepos> searchReposObservable = searchReposObservable(text);
        if (searchReposObservable != null) {
            subscription = searchReposObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(repos -> repos.getItems())
                    .flatMap(items -> Observable.from(items))
                    .flatMap(item -> Helpers.getOnlyName(item))
                    .map(name -> searchRepos.add(name))
                    .subscribe(new Subscriber<Boolean>() {
                        @Override
                        public void onCompleted() {
                            Log.e(TAG, "!!!onCompleted searchRepos = " + searchRepos);
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = "";
                            try {
                                if (e instanceof IOException) {
                                    message = "No internet connection!";
                                } else if (e instanceof HttpException) {
                                    HttpException error = (HttpException) e;
                                    Response response = error.response();
                                    String errorBody = response.errorBody().string();

                                    GsonBuilder gsonBuilder = new GsonBuilder();
                                    Gson gson = gsonBuilder.create();
                                    Errors errors = gson.fromJson(errorBody, Errors.class);
                                    Log.e(TAG, "!!!! error.code() = " + error.code() + ", errorBody = " + errorBody);

                                    switch (response.code()) {
                                        case 422:
                                            message = errors.getError().getCode();
                                            break;
                                        case 400:
                                            message = errors.getMessage();
                                            break;
                                        default:
                                            message = errorBody.toString();
                                            break;
                                    }
                                }
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }

                            if (TextUtils.isEmpty(message)) {
                                message = "Unknown error occurred! Check LogCat.";
                            }
                            Log.e(TAG, "!!!! message = " + message);
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
                        }

                        @Override
                        public void onNext(Boolean aBoolean) {
                            Log.e(TAG, "!!!onNext aBoolean = " + aBoolean);
                            Log.e(TAG, "!!!onCompleted searchRepos = " + searchRepos);
                            notifyListForAdapter(searchRepos);
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.unknown_error), Toast.LENGTH_LONG).show();
        }
    }
}
