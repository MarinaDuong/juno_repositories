package com.example.lenovo.junorepositories.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.lenovo.junorepositories.entities.Item;
import com.example.lenovo.junorepositories.entities.JunoRepositoriesEntity;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by Lenovo on 24.05.2018.
 */

public class Helpers {
    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        //get all networks information
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = cm.getAllNetworks();
            int i;

            //checking internet connectivity
            for (i = 0; i < networks.length; ++i) {
                if (cm.getNetworkInfo(networks[i]).getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
            return false;
        } else {
            NetworkInfo wifiInfo = cm.getActiveNetworkInfo();
            return wifiInfo != null;
        }
    }

    public static Observable<String> getName(JunoRepositoriesEntity junoRepositoriesEntity) {
        return Observable.just(junoRepositoriesEntity.getName());
    }
    public static Observable<String> getOnlyName(Item entity) {
        return Observable.just(entity.getName());
    }
}
