package com.example.lenovo.junorepositories.rxrest;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by Lenovo on 05.07.2018.
 */

public class RxRetrofitRestService {

    public static RepositoriesApi getRepositoriesApi() {

        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
        repositoriesApi = retrofit.create(RepositoriesApi.class);
        return repositoriesApi;
    }

    private static RepositoriesApi repositoriesApi;

    private RxRetrofitRestService() {

    }

}
