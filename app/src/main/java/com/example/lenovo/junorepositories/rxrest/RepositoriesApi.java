package com.example.lenovo.junorepositories.rxrest;

import com.example.lenovo.junorepositories.entities.JunoRepositoriesEntity;
import com.example.lenovo.junorepositories.entities.SearchRepos;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Lenovo on 24.05.2018.
 */

public interface RepositoriesApi {
    @GET("/orgs/{org}/repos")
    Observable<ArrayList<JunoRepositoriesEntity>> getRepositories(@Path("org") String org, @Query("type") String type, @Query("page") int page);

    @GET
    Observable<SearchRepos> getSearchRepos(@Url String url);

//    @GET("/search/repositories")
//    Observable<SearchRepos> getSearchRepos(@Query("org") String org);
}
