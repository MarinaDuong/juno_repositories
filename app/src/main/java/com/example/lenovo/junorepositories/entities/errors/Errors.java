package com.example.lenovo.junorepositories.entities.errors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Errors {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("errors")
    @Expose
    private Error error;

    public String getMessage() {
        return message;
    }

    public Error getError() {
        return error;
    }

}
