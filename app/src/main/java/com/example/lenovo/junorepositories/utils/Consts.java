package com.example.lenovo.junorepositories.utils;

/**
 * Created by Lenovo on 06.07.2018.
 */

public class Consts {
    public static final String ORG = "gojuno";
    public static final String TYPE = "sources";
    public static final String SEARCH_REPOS_URL = "/search/repositories?q=org:gojuno+%1$s in:name";
}
